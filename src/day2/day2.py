#!/usr/bin/env python3

import sys
import re

test_list = [("1-3 a", "abc"), ("1-3 a", "def")]


def read_input(filename):
    global test_list
    with open(filename) as input_file:
        test_list = [line.rstrip().split(":") for line in input_file]

def match_password(min, max, char, password):
    if password == None:
        return False

    char_count = password.count(char)
    if char_count >= min and char_count <= max:
        return True

    return False

def match_password2(first, second, char, password):
    if password == None:
        return False

    first_char=password[first]
    second_char=password[second]
    if bool(first_char == char) ^ bool(second_char == char):
        return True

    return False

def match_password_pattern(pattern, password, algorythm):
    # pattern: 1-3 x
    # min value is index 0
    # max value is index 2
    # character is index 4
    pattern_splitted=re.split('(\d+)\-(\d+) ([a-zA-Z])', pattern)
    min = int(pattern_splitted[1])
    max = int(pattern_splitted[2])
    char = pattern_splitted[3]
    return algorythm(min, max, char, password)

def main(argv):
    print("Day 2")

    if len(argv) == 0:
        print("Give filename as first param")
        return
    
    file_name = argv[0]
    print("Read input from %s" % file_name)
    read_input(file_name)

    print("Part 1 result")    
    pwd_matches = list(map(lambda combi: match_password_pattern(combi[0], combi[1], match_password), test_list))
    print(pwd_matches.count(True))

    print("Part 2 result")
    pwd_matches = list(map(lambda combi: match_password_pattern(combi[0], combi[1], match_password2), test_list))
    print(pwd_matches.count(True))


if __name__ == '__main__':
    main(sys.argv[1:])