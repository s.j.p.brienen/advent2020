#!/usr/bin/env python3

import sys
import re
import math

map_pattern = []
map_size = [0,0]
the_map = []
tree = "#"

multiplied_result=1

steps = [(1,1),(3,1),(5,1),(7,1),(1,2)]

def read_input(filename):
    global map_pattern
    with open(filename) as input_file:
        map_pattern = [line.rstrip() for line in input_file]

def extend_map(step_x):
    global the_map
    desired_map_width = (map_size[1] - 1) * step_x
    number_of_extensions = math.ceil(desired_map_width / map_size[0])
    the_map = list(map(lambda l: "".join([l] * number_of_extensions), map_pattern))
    update_map_size()
    
def update_map_size():
    global map_size
    map_size[0] = len(the_map[0]) # x axis -- width
    map_size[1] = len(the_map)    # y axis -- height

def find_trees(step):
    global multiplied_result
    global the_map

    step_x = step[0]
    step_y = step[1]

    the_map = map_pattern
    update_map_size()
    extend_map(step_x)

    the_path = ""
    cur_width=0
    cur_height=0
    while cur_height < len(the_map) - 1:
        cur_height += step_y
        cur_width += step_x
        the_path += the_map[cur_height][cur_width]

    num_trees = the_path.count(tree)
    multiplied_result *= num_trees
    print("# trees for (%d, %d): %d" % (step_x, step_y, num_trees))

def main(argv):
    print("Day 3")

    if len(argv) == 0:
        print("Give filename as first param")
        return
    
    file_name = argv[0]
    print("Read input from %s" % file_name)
    read_input(file_name)
    if len(map_pattern) == 0:
        print("Pattern empty.")
        return

    for step in steps:
        find_trees(step)

    print(multiplied_result)

if __name__ == '__main__':
    main(sys.argv[1:])