#!/usr/bin/env python3

import sys
from itertools import combinations

test_list = [1721, 979, 366, 299, 675, 1456]

pairs = []
triples = []

def read_input(filename):
    global test_list
    with open(filename) as input_file:
        test_list = [int(line.rstrip()) for line in input_file]

def make_pairs(input_list):
    global pairs
    pairs = [comb for comb in combinations(input_list, 2)]

def make_triples(input_list):
    global triples
    triples = [comb for comb in combinations(input_list, 3)]

def find_correct_triple(input_list):
    sums=list(map(lambda x: x[0] + x[1] + x[2], input_list))
    try:
        return sums.index(2020)
    except:
        return -1

def find_correct_pair(input_list):
    sums=list(map(lambda x: x[0] + x[1], input_list))
    try:
        return sums.index(2020)
    except:
        return -1

def main(argv):
    print("Day 1")

    if len(argv) == 0:
        print("Give filename as first param")
        return
    
    file_name = argv[0]
    print("Read input from %s" % file_name)
    read_input(file_name)
    
    make_pairs(test_list)
    
    correct_index = find_correct_pair(pairs)
    if correct_index >= 0:
        correct_pair = pairs[correct_index]
        print('%d * %d = %d' % (correct_pair[0], correct_pair[1], correct_pair[0] * correct_pair[1]))
    else:
        print('No pair found!')

    make_triples(test_list)
    correct_index = find_correct_triple(triples)
    if correct_index >= 0:
        correct_triple = triples[correct_index]
        print('%d * %d * %d = %d' % (correct_triple[0], correct_triple[1], correct_triple[2], correct_triple[0] * correct_triple[1] * correct_triple[2]))
    else:
        print('No triple found!')


if __name__ == '__main__':
    main(sys.argv[1:])